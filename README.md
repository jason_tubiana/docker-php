# Docker php

## Description

Docker set-up to manage php project.

## Container
* php
* nginx
* mariadb
* adminer
* phpspec

## How to install ?

```bash
git clone https://github.com/dreudi13/docker-php <directory>
```

## How to use ?
Create a directory named application.

```bash
mkdir application
```

The target of nginx in './application/public', so create this directory and create index.php.

```bash
mkdir application/public && touch application/public/index.php
```

```php
# ./application/public/indes.php

<?php

$success = false;
$error = false;

try {
    $pdo = new \PDO('mysql:dbname=example;host=mariadb', 'root', 'root');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $success = true;
} catch (\PDOException $e) {
    $error = $e->getMessage();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My app</title>
    <style>
        *,
        *::before,
        *::after {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }
        body {
            color: #222;
            font-family: sans-serif;
        }
        main {
            margin: 0 auto;
            width: 60%;
        }
        h1 {
            padding-top: 1rem;
            padding-bottom: .5rem;
            font-weight: 500;
            border-bottom: 1px solid #222;
        }
        section {
            margin-top: 2rem;
            margin-left: 1rem;
        }
        article {
            margin-top: 2rem;
        }
        article h1 {
            font-size: 3rem;
        }
        article h2 {
            margin-top: 1rem;
        }
    </style>
</head>
<body>

    <main>

        <?php if ($error !== false): ?>
            <h1><?= dirname(__DIR__) ?></h1>
            <br>
            <h2>Database connection error : </h2>
            <br>
            <p>
                <?= $error ?>
            </p>
            <br>
            <?php phpinfo(); ?>
        <?php endif; ?>

        <?php if ($success): ?>
            <h1>Database connection is ok!</h1>
            <h2><?= dirname(__DIR__) ?></h2>
        <?php endif; ?>

    </main>

</body>
</html>
```

That's it !
Now replace with your own code.
Enjoy!
